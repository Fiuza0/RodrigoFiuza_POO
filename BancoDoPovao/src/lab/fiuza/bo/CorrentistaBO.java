package lab.fiuza.bo;

import java.util.List;

import lab.fiuza.dao.CorrentistaDAO;
import lab.fiuza.domain.Correntista;

public class CorrentistaBO {

	public CorrentistaDAO correntista = new CorrentistaDAO();

	public static String incluirLista(Correntista correntista) {
		String erro = validar(correntista);
		if(erro != null) {
			return erro;
		}
		return CorrentistaDAO.incluirNaLista(correntista);
	}

	private static String validar(Correntista correntista) {
		if (correntista.getNome().trim().isEmpty()) {
			return "ERRO: nome n�o informado.";
		}
		if (correntista.getRg().trim().isEmpty()) {
			return "ERRO: RG n�o informado.";
		}
		if (correntista.getComprovanteRenda().trim().isEmpty()) {
			return "ERRO:Comprovante de Renda n�o Informado";
		}
		if (correntista.getComprovanteResidencia().trim().isEmpty()) {
			return "ERRO:Comprovante de Residencia n�o Informado";
		}
		return null;

	}
	public static List<Correntista> obterTodos() {
		return CorrentistaDAO.obterTodos();
	}


}
