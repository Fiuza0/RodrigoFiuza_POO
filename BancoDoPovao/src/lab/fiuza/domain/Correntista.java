package lab.fiuza.domain;

public class Correntista {

	private String nome;

	private String cpf;

	private int telefone;

	private String endereco;

	private float rendaMensal;

	// DOCUMENTOS EXIGIDOS

	private String rg;

	private String comprovanteResidencia;

	private String comprovanteRenda;
	
	//Saldo
	private double saldo;

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Correntista(String nome, String cpf, int telefone, String endereco, float rendaM, String rg,
			String comprovanteResidencia, String comprovanteRenda, double saldo) {
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.endereco = endereco;
		this.rendaMensal = rendaM;
		this.rg = rg;
		this.comprovanteResidencia = comprovanteResidencia;
		this.comprovanteRenda = comprovanteRenda;
		this.saldo = saldo;

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public int getTelefone() {
		return telefone;
	}

	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public float getRendaMensal() {
		return rendaMensal;
	}

	public void setRendaMensal(float rendaMensal) {
		this.rendaMensal = rendaMensal;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getComprovanteResidencia() {
		return comprovanteResidencia;
	}

	public void setComprovanteResidencia(String comprovanteResidencia) {
		this.comprovanteResidencia = comprovanteResidencia;
	}

	public String getComprovanteRenda() {
		return comprovanteRenda;
	}

	public void setComprovanteRenda(String comprovanteRenda) {
		this.comprovanteRenda = comprovanteRenda;
	}

}
