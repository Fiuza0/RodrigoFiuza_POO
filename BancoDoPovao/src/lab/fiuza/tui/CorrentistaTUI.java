package lab.fiuza.tui;

import java.util.List;
import java.util.Scanner;

import lab.fiuza.bo.CorrentistaBO;
import lab.fiuza.domain.Correntista;

public class CorrentistaTUI {
	
	public static Scanner scanner = new Scanner(System.in);

	public static void inserirCorrentista() {
		
		System.out.println("############ INCLUS�O DE CORRENTISTA ############");
		String nome = obterTexto("Digite seu nome: ");
		String cpf = obterTexto("Digite seu CPF: ");
		int telefone = obterInteiro("Digite seu N�mero de Contato");
		String endereco = obterTexto("Digite seu endereco");
		float rendaM = obterFloat("Digite sua Renda M�dia mensal");
		String rg = obterTexto("Digite seu RG");
		String comprovanteRes = obterTexto("Coloque seu comprovante de Residencia");
		String comprovanteRenda = obterTexto("Coloque seu comprovante de Renda");
		double saldo = 0;
		Correntista correntistas = new Correntista(nome, cpf, telefone, endereco, rendaM, rg, comprovanteRes,
				comprovanteRenda, saldo);
		String erro = CorrentistaBO.incluirLista(correntistas);
		
		if (erro != null) {
			System.out.println("No foi possvel criar.");
			System.out.println(erro);
		}


	}
	public static void listarDados() {
		System.out.println("############ LISTAGEM DE DADOS ############");
		List<Correntista> correntistas = CorrentistaBO.obterTodos();

		for (Correntista correntista : correntistas) {
			System.out.println("Dados da conta");
			System.out.println(" \t" + correntista.getNome());
			System.out.println(" \t" + correntista.getCpf());
			System.out.println(" \t" + correntista.getTelefone());
			System.out.println(" \t" + correntista.getEndereco());
			System.out.println(" \t" + correntista.getRendaMensal());
			System.out.println(" \t" + correntista.getRg());
			System.out.println(" \t" + correntista.getComprovanteResidencia());
			System.out.println(" \t" + correntista.getComprovanteRenda());
			
		}
	}
	public void deposito() {
		double deposito = obterDouble("Quanto deseja depositar? : ");
		
	}
	public void saque() {
		double saque = obterDouble("Quanto deseja depositar? : ");
		
	}
	public void bloqueioConta() {
		
	}
	public void encerramentoConta() {
		
	}
	public void desbloqueioConta() {
		
	}

	private static String obterTexto(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextLine();
	}

	private static Integer obterInteiro(String mensagem) {
		System.out.println(mensagem);
		Integer numero = scanner.nextInt();
		scanner.nextLine();
		return numero;
	}

	private static float obterFloat(String mensagem) {
		System.out.println(mensagem);
		float numero = scanner.nextFloat();
		scanner.nextLine();
		return numero;
	}
	private static double obterDouble(String mensagem) {
		System.out.println(mensagem);
		double numero = scanner.nextDouble();
		scanner.nextLine();
		return numero;
	}

}
