package lab.fiuza.domain;

import java.util.ArrayList;
import java.util.List;

import lab.fiuza.tui.ContratoTui;

public class Contrato {

	Integer numeroContrato;
	String nome;
	String endereco;
	List <Veiculo> veiculos;
	double valor;
	
	
	public void setVeiculosContrato(List<Veiculo> veiculosContrato) {
		this.veiculos = veiculosContrato;
	}
	public Integer getNumeroContrato() {
		return numeroContrato;
	}
	public void setNumeroContrato(Integer numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getNomeCliente() {
		return nome;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nome = nomeCliente;
	}
	public String getEnderecoCliente() {
		return endereco;
	}
	public void setEnderecoCliente(String enderecoCliente) {
		this.endereco = enderecoCliente;
	}	
	public double getValorContrato() {
		return valor;
	}
	public List<Veiculo> getVeiculosContrato() {
		return veiculos;
	}
	public void setValorContrato(double valorContrato) {
		this.valor = valorContrato;
	}
	
	
}
