package lab.fiuza.domain;

import lab.fiuza.enums.Enumeracao;

public class Veiculo {

	
	String placa;
	long anoFabricacao;
	Enumeracao tipo;
	double valor;
	
	
	public double getValorVeiculo() {
		return valor;
	}
	public void setValorVeiculo(double valorVeiculo) {
		this.valor = valorVeiculo;
	}
	public Enumeracao getTipoVeiculo() {
		return tipo;
	}
	public void setTipoVeiculo(Enumeracao tipoVeiculo) {
		this.tipo = tipoVeiculo;
	}
	
	
	public String getPlacaVeiculo() {
		return placa;
	}
	public void setPlacaVeiculo(String placaVeiculo) {
		this.placa = placaVeiculo;
	}
	public long getAnoFabricacao() {
		return anoFabricacao;
	}
	public void setAnoFabricacao(long anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	
}
