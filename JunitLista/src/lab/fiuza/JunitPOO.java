package lab.fiuza;

import java.util.Scanner;

public class JunitPOO {
	public static void main(String[] args) {
		int[] vet = new int[5];
		obterNumeros(vet);
		exibirMaiorNumero(encontrarMaiorNumero(vet));

	}

	private static void exibirMaiorNumero(int maior) {
		System.out.println("O numero maior �: "+maior);

	}

	private static int encontrarMaiorNumero(int[] vet) {
		int maiorNum=0;
		for(int i = 0;i< vet.length;i++) {
			if(i>0) {
				if(vet[i] > vet[i-1]) {
					maiorNum = vet[i];
				}
			}

		}

		return maiorNum;

	}

	private static void obterNumeros(int[] vet) {
		int cont = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe 5 Valores:");
		do {
			vet[cont] = sc.nextInt();
			cont++;
		}while(cont < vet.length);

	}
}
